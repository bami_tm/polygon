import 'package:flutter/material.dart';
import 'package:polygon_clipper/polygon_clipper.dart';

void main() {
  Paint.enableDithering = true;
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Spacer(
            flex: 1,
          ),
          Center(
              child: ClipPolygon(
                  sides: 6,
                  borderRadius: 20.0, // Default 0.0 degrees
                  rotate: 90.0, // Default 0.0 degrees
                  boxShadows: [
                    PolygonBoxShadow(
                        color: Colors.lightBlueAccent, elevation: 20.0),
                    PolygonBoxShadow(
                        color: Colors.lightBlueAccent, elevation: 50.0)
                  ],
                  child: Container(
                    color: Color(0xffeaf7ff),
                    padding: const EdgeInsets.all(20.0),
                    child: ClipPolygon(
                      sides: 6,
                      borderRadius: 20.0, // Default 0.0 degrees
                      rotate: 90.0, // Default 0.0 degrees
                      boxShadows: [
                        PolygonBoxShadow(
                            color: Color(0xffeaf7ff), elevation: 20.0),
                        PolygonBoxShadow(
                            color: Color(0xffeaf7ff), elevation: 50.0)
                      ],
                      child: Image(
                          fit: BoxFit.fill,
                          image: NetworkImage(
                              'https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885__480.jpg')),
                    ),
                  ))),
          Spacer(
            flex: 1,
          ),
          Container(
            color: Colors.black,
            padding:
                EdgeInsets.only(top: 8.0, bottom: 8.0, left: 15, right: 15),
            child: DecoratedBox(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xFFfa4a9b),

                        Color(0xFF852ebe),
                        //add more colors
                      ],
                      begin: FractionalOffset.topCenter,
                      end: FractionalOffset.bottomCenter,
                    ),
                    borderRadius: BorderRadius.circular(25),
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color:
                              Color.fromRGBO(0, 0, 0, 0.57), //shadow for button
                          blurRadius: 5) //blur radius of shadow
                    ]),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      maximumSize: Size(140, 60),
                      minimumSize: Size(140, 60),
                      primary: Colors.transparent,
                      onSurface: Colors.transparent,
                      shadowColor: Colors.transparent,
                      //make color or elevated button transparent
                    ),
                    onPressed: () {
                      print("You pressed Elevated Button");
                    },
                    child: Padding(
                      padding: EdgeInsets.only(
                        top: 18,
                        bottom: 18,
                      ),
                      child: Text(
                        "Verify",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 20),
                      ),
                    ))),
          ),
          Spacer(
            flex: 1,
          ),
        ],
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
